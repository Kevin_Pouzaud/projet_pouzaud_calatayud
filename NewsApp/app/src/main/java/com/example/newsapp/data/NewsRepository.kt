package com.example.newsapp.data

import com.example.newsapp.data.remote.NewsRemoteDataSource

class NewsRepository(private  val newsDataSource: NewsRemoteDataSource){
    suspend fun getNews() = newsDataSource.getNews()
}
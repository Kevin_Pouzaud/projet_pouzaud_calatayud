package com.example.newsapp

import android.app.Application
import com.example.newsapp.data.NewsRepository
import com.example.newsapp.data.remote.NewsRemoteDataSource

class App : Application() {

    companion object {
        lateinit var instance: App

        val newsRepository by lazy {
            NewsRepository(NewsRemoteDataSource)
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}
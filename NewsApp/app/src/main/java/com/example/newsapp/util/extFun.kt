package com.example.newsapp.util

import androidx.fragment.app.Fragment
import com.example.newsapp.App

fun Fragment.getViewModelFactory(): ViewModelFactory {
        val repository = App.newsRepository
        return ViewModelFactory(repository, this)
    }